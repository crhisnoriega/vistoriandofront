import React, { Component } from "react";

import {
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
}
from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser } from "../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: {
        name: "",
        password: "",
        password: "",
        phone: "",
        email: ""
      },
      isEdit: false
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    this.setState({ show: true });
    if (this.props.session.userEdit) {
      this.setState({
        init: this.props.session.userEdit,
        administrator: {
          value: this.props.session.userEdit.administrator,
          label: this.props.session.userEdit.administrator ? "Sim" : "Não"
        },
        deviceReadonly: {
          value: this.props.session.userEdit.deviceReadonly,
          label: this.props.session.userEdit.deviceReadonly ? "Sim" : "Não"
        },
        disabled: {
          value: this.props.session.userEdit.disabled,
          label: this.props.session.userEdit.disabled ? "Sim" : "Não"
        },
        readonly: {
          value: this.props.session.userEdit.readonly,
          label: this.props.session.userEdit.readonly ? "Sim" : "Não"
        },
        isEdit: true
      });
    }
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do usuario"),
      password: Yup.string().required("senha obrigatoria"),
      email: Yup.string().required("email obrigatorio"),
      phone: Yup.string().required("numero de telefone obrigatorio")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do usuario"),
      email: Yup.string().required("email obrigatorio"),
      phone: Yup.string().required("numero de telefone obrigatorio")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      }
      catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    values.administrator = this.state.administrator.value;
    values.readonly = this.state.readonly.value;
    values.disabled = this.state.disabled.value;
    values.deviceReadonly = this.state.deviceReadonly.value;

    console.log(values);

    if (this.state.isEdit) {
      values.id = this.props.session.userEdit.id;
      values.attributes = this.props.session.userEdit.attributes;

      if (values.attributes == null) {
        values.attributes = {};
      }
      values.attributes.vistoriador = this.state.viewer.value;

      axios
        .post(
          "/api/putapicall", {
            url: "/users/" + this.props.session.userEdit.id,
            data: values,
            headers: {
              "Content-Type": "application/json",
              Authorization: "Basic " + this.props.session.token
            }
          }, {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          this.props.setEditUser(null);
          this.setState({
            deviceReadonly: {},
            disabled: {},
            readonly: {},
            administrator: {},
            init: {
              name: "",
              email: "",
              password: "",
              password: "",
              phone: ""
            },
            isEdit: false
          });

          setSubmitting(false);
        });
    }
    else {
      if (values.attributes == null) {
        values.attributes = {};
      }
      values.attributes.vistoriador = this.state.viewer.value;

      axios
        .post(
          "/api/apicall", {
            url: "/users",
            data: values,
            headers: {
              "Content-Type": "application/json",
              Authorization: "Basic " + this.props.session.token
            }
          }, {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          this.props.setEditUser(null);
          this.setState({
            deviceReadonly: {},
            disabled: {},
            readonly: {},
            administrator: {},
            init: {
              name: "",
              email: "",
              password: "",
              password: "",
              phone: ""
            },
            isEdit: false
          });
          setSubmitting(false);
        });
    }
    return;
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  render() {
    var fuels = ["Flex", "Gasolina", "Alcool", "Diesel", "GNV"];
    var fuels2 = fuels.map(item => {
      return { value: item, label: item };
    });

    var _init = this.state.init;

    return (
      <div>
        <Loading show={this.state.show} showSpinner={false} />
        <Card>

          <CardBody>
            <Formik
              enableReinitialize
              initialValues={_init}
              validate={this.myValidator(
                this.state.isEdit
                  ? this.validationSchemaEdit
                  : this.validationSchemaComplete
              )}
              onSubmit={this.handleSubmit}
              render={({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                setTouched
              }) => (
                <Container>
               
                  <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                   <Card>
                   <CardHeader>
                   Dados
                   </CardHeader>
                <CardBody>
                
                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                      <FormFeedback>{errors.name}</FormFeedback>
                    </FormGroup>
                
                 <Row form className="col-md-15">
                    <FormGroup  className="col-md-4">
                      <Label className="green-label" for="email">
                        CPF (acesso)
                      </Label>
                       <Input
                        
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                          
                          mask="999.999.999-99"
                          maskChar=" "
                          tag={InputMask}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    <FormGroup  className="col-md-4">
                      <Label className="green-label" for="email">
                        RG
                      </Label>
                       <Input
                        
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                          
                          mask="999.999.999-99"
                          maskChar=" "
                          tag={InputMask}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    </Row>
                    
                     <FormGroup>
                      <Label className="green-label" for="email">
                        Orgao Emissor
                      </Label>
                       <Input
                         className="col-md-4"
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    
                    <Row form className="col-md-15">

                     <FormGroup className="col-md-3">
                        <Label className="green-label" for="admin">
                         Sexo
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.deviceReadonly}
                          options={[
                            { value: true, label: "Homem" },
                            { value: false, label: "Mulher" }
                          ]}
                          onChange={deviceReadonly => {
                            errors.deviceReadonly = "";
                            if (deviceReadonly == null) {
                              deviceReadonly = "";
                            }
                            this.setState({ deviceReadonly });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>
                      
                      <FormGroup>
                      <Label className="green-label" for="email">
                        Naturalidade
                      </Label>
                       <Input
                         className="col-md-10"
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                  </Row>
                  
                   <Row form className="col-md-15">
                  <FormGroup  className="col-md-4">
                      <Label className="green-label" for="email">
                        Nacionalidade
                      </Label>
                       <Input
                        
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    <FormGroup className="col-md-4">
                      <Label className="green-label" for="email">
                        Estado Civil
                      </Label>
                       <Input
                        
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                  </Row>
                  
                  
                   <FormGroup>
                      <Label className="green-label" for="email">
                        Observação
                      </Label>
                       <Input
                         className="col-md-10"
                          name="user"
                          type="textarea"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    <FormGroup>
                      <Label className="green-label" for="photo">
                        Imagem
                      </Label>
                      {this.state.image ? (
                        <img src={this.state.image} />
                      ) : (
                        <div className="sem-imagem col-md-4">sem imagem</div>
                      )}
                      <input
                        className="col-md-6" style={{marginTop:"10px"}}
                        type="file"
                        onChange={e => this.handleFileSelect(e)}
                      />
                      <FormFeedback>{errors.photo}</FormFeedback>
                    </FormGroup>
                    </CardBody>
                </Card>
                
                <Card>
                  <CardHeader>
                  Funcionario
                  </CardHeader>
                <CardBody>
                  
                  
              
                    
                  <FormGroup className="col-md-8">
                        <Label className="green-label" for="admin">
                         Expediente
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.deviceReadonly}
                          options={[
                            { value: true, label: "Selecione um expediente" },
                            { value: false, label: "Padrão" }
                          ]}
                          onChange={deviceReadonly => {
                            errors.deviceReadonly = "";
                            if (deviceReadonly == null) {
                              deviceReadonly = "";
                            }
                            this.setState({ deviceReadonly });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>
                    </CardBody>
                  </Card>
           
           
                <Card>
                  <CardHeader>
                  Endereço
                  </CardHeader>
                <CardBody>
                    
            <Row form className="col-md-15">
                  <FormGroup className="col-3">
                      <Label className="green-label" for="email">
                        CEP
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    <FormGroup className="col-6">
                      <Label className="green-label" for="email">
                        Logradouro
                      </Label>
                       <Input
                        
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    <FormGroup className="col-2">
                      <Label className="green-label" for="email">
                        Numero
                      </Label>
                       <Input
                        
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                  </Row>
                  
                   <Row form className="col-md-15">
                  <FormGroup className="col-3">
                      <Label className="green-label" for="email">
                        Complemento
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    </Row>
                    
                    
                     <Row form className="col-md-15">
                  <FormGroup className="col-5">
                      <Label className="green-label" for="email">
                        Bairro
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    <FormGroup className="col-5">
                      <Label className="green-label" for="email">
                        Cidade
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    </Row>
                   
                   <Row form className="col-md-15">
                  <FormGroup className="col-3">
                      <Label className="green-label" for="email">
                        Estado
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    </Row>
                    
                     </CardBody>
                </Card>
                
                 
                <Card>
                <CardHeader>
                  Contato
                  </CardHeader>
                <CardBody>
                    
                     <Row form className="col-md-15">
                  <FormGroup className="col-5">
                      <Label className="green-label" for="email">
                        Telefone 1
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    <FormGroup className="col-5">
                      <Label className="green-label" for="email">
                        Telefone 2
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    </Row>
                    
                    
                     <Row form className="col-md-15">
                  <FormGroup className="col-8">
                      <Label className="green-label" for="email">
                        Email
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    </Row>
                </CardBody>
                </Card>
                
                
                 <Card>
                <CardHeader>
                  Acesso ao Aplicativo
                  </CardHeader>
                <CardBody>
                    
                     <Row form className="col-md-15">
                  <FormGroup className="col-5">
                      <Label className="green-label" for="email">
                        Senha
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    <FormGroup className="col-5">
                      <Label className="green-label" for="email">
                        Confirme a senha
                      </Label>
                       <Input
                         
                          name="user"
                          type="text"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            
                          }}
                        />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>
                    
                    </Row>
                    
                    
                    
                </CardBody>
                </Card>
                
                
                    <Row className="justify-content-sm-start">
                      <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <LaddaButton
                          type="submit"
                          className="col-6 btn btn-primary btn-ladda"
                          loading={isSubmitting}
                          onClick={e => {
                            handleSubmit(e);
                            this.toggleBtn("expLeft");
                          }}
                        >
                          Enviar
                        </LaddaButton>
                      </Col>
                    </Row>
                    
                  </Form>
                </Container>
              )}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
