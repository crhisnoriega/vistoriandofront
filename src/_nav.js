export default {
  items: [
    {
      name: "Dashboard",
      url: "/app/dashboard",
      icon: "icon-chart",
      badge: {
        variant: "info",
        text: ""
      }
    },
    {
      title: true,
      name: "Vistoria",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "" // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Calendar",
      url: "/app/calendar",
      icon: "icon-chart",
      badge: {
        variant: "info",
        text: ""
      }
    },
    {
      title: true,
      name: "Empresa",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "" // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Acessos",
      url: "/users",
      icon: "icon-directions",
      children: [
        {
          name: "Cadastro",
          url: "/app/users/register",
          icon: "icon-note"
        },
        {
          name: "Lista",
          url: "/app/users/list",
          icon: "icon-list"
        }
      ]
    },

    {
      name: "Vistoriadores",
      url: "/surveyors",
      icon: "icon-people",
      children: [
        {
          name: "Cadastro",
          url: "/app/surveyors/register",
          icon: "icon-note"
        },
        {
          name: "Lista",
          url: "/app/surveyors/list",
          icon: "icon-list"
        }
      ]
    },

    {
      name: "Imobiliarias",
      url: "/agency",
      icon: "icon-home",
      children: [
        {
          name: "Cadastro",
          url: "/app/agency/register",
          icon: "icon-note"
        },
        {
          name: "Lista",
          url: "/app/agency/list",
          icon: "icon-list"
        }
      ]
    }
  ]
};
