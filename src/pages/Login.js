import { withRouter, Link } from "react-router-dom";
import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Media,
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
}
from "reactstrap";

import axios from "axios";
import history from "../history/history";
import base64 from "react-native-base64";

import logo from "../assets/img/brand/vistoriando_logo.jpeg";

import { connect } from "react-redux";
import { activateGeod, closeGeod, doLogin, setToken } from "../redux/redux";

import StringMask from "string-mask";
import InputMask from "react-input-mask";

import scriptLoader from "react-async-script-loader";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      password: "",
      validate: {
        invaliduser: false,
        invalidpassword: false
      },
      showerror: false,
      message: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(e) {
    console.log(this.state);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  loadJS = function(src) {
    var tag = document.createElement("script");
    tag.async = false;
    tag.type = "text/javascript";
    tag.src = src;
    document.getElementsByTagName("body")[0].appendChild(tag);
  };

  loadCSS = function(src) {
    var tag = document.createElement("script");
    tag.async = false;
    tag.type = "text/javascript";
    tag.src = src;
    document.getElementsByTagName("body")[0].appendChild(tag);
  };

  componentWillReceiveProps({ isScriptLoaded, isScriptLoadSucceed }) {}

  componentDidMount() {}

  routeChange(e) {
    e.preventDefault();
    console.log(this.state.user);
    console.log(this.state.password);

    let login = this.state.user.replace(/\./g, "").replace(/\-/g, "");

    if (login == "23135250899" && this.state.password == "123456") {
      history.push("/app");
    }
    else {
      this.setState({ showerror: true, message: "Usuario ou senha incorreta" });
    }

    axios
      .post(
        "/user/auth", {
          login: "",
          password: ""
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data.error != null) {
          this.setState({ showerror: true, message: res.data.error });
          return;
        }

        if (res.data.id != null) {
          this.props.doLogin(res.data);
          this.props.setToken(
            base64.encode(this.state.user + ":" + this.state.password)
          );
          history.push("/app");
          return;
        }
      });
  }

  handleClose(e) {
    this.setState({ showerror: false, showerrornetwork: false });
  }

  render() {
    return (
      <div id="login_body" className="login_app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card className="p-2">
                  <CardBody>
                    <Form className="form" onSubmit={e => this.routeChange(e)}>
                      <Row>
                        <Media
                          style={{
                            display: "table",
                            margin: "0 auto",
                            width: "110px",
                            height: "95px"
                          }}
                          object
                          src={logo}
                          alt="Generic placeholder image"
                        />
                      </Row>
                      <h1
                        style={{ display: "table", margin: "0 auto" }}
                        color="primary"
                        className="my-title-2"
                      />
                      <p
                        style={{ display: "table", margin: "0 auto" }}
                        className="text-muted"
                      >
                        Entre na sua conta
                      </p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="user"
                          type="text"
                          placeholder="CPF"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            this.handleInputChange(e);
                          }}
                          invalid={this.state.validate.invaliduser}
                          mask="999.999.999-99"
                          maskChar=" "
                          tag={InputMask}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="password"
                          type="password"
                          placeholder="Senha"
                          autoComplete="current-password"
                          value={this.state.password}
                          onChange={e => {
                            this.handleInputChange(e);
                          }}
                          invalid={this.state.validate.invalidpassword}
                        />
                      </InputGroup>
                      <Row>
                        <Button
                          style={{ display: "table", margin: "0 auto" }}
                          color="primary"
                          className="px-4"
                        >
                          Entrar
                        </Button>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>{this.state.message}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerrornetwork}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>Erro inesperado</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  activateGeod,
  closeGeod,
  doLogin,
  setToken
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
