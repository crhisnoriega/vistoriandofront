import React, { Component } from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setInvestidor } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRowWithSelect from "./UserRowWithSelect";
import DeviceRowWithSelect from "./DeviceRowWithSelect";
import DeviceRow from "./DeviceRow";

import "spinkit/css/spinkit.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

class UserPermissions extends Component {
  constructor(props) {
    super(props);
    this.selUsers = {};
    this.selDevices = [];

    this.state = {
      filter: "",
      showprogress: false,
      users: [],
      devices: [],
      submiting: false
    };
  }

  componentDidMount() {
    this.setState({ showprogress: true });

    axios
      .post(
        "/api/getapicall",
        {
          url: "/users",
          data: "",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data.error != null) {
          alert(JSON.stringify(res.data.error));
        } else {
          this.setState({
            users: res.data
          });
        }
        this.setState({
          showprogress: false,
          showusers: true
        });
      });
  }

  searchUsers(filter) {
    this.setState({ showprogress: true });
    axios
      .post(
        "/api/getapicall",
        {
          url: "/users/search",
          data: "?query=" + filter,
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data.error != null) {
          alert(JSON.stringify(res.data.error));
        } else {
          this.setState({
            users: res.data
          });
        }
        this.setState({
          showprogress: false,
          showusers: true
        });
      });
  }

  searchDevices(filter) {
    this.setState({ showprogress: true });
    this.selDevices = [];
    axios
      .post(
        "/api/getapicall",
        {
          url: "/devices/search",
          data: "?query=" + filter,
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data != null) {
          this.setState({
            devices: res.data,
            showprogress: false,
            showdevices: true
          });
        }
      });
  }

  onChangeUser = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUsers(filter);
        } else {
          _this.setState({ users: [] });
        }
      }, 1000)
    });
  };

  onChangeDevice = e => {
    this.setState({ filter: e.target.value });
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchDevices(filter);
        } else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.selUsers[user.id] = user;
  };

  callbackDevice = (device, checked) => {
    if (checked) {
      this.selDevices.push(device);
    } else {
      this.selDevices = this.selDevices.filter(d => d.id != device.id);
    }
  };

  toggleBtn(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  }

  handleChange = selectedOptions => {
    this.setState({ techFilter: selectedOptions });
  };

  doLinkPermission = (userid, deviceid) => {
    axios
      .post(
        "/api/apicall",
        {
          url: "/permissions",
          data: { userId: userid, deviceId: deviceid },
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        this.setState({
          submiting: false,
          devices: [],
          userOp: [],
          filter: ""
        });
      });
  };

  doSubmit = async () => {
    this.setState({
      submiting: true
    });

    console.log(this.state.userOp);
    console.log(this.selDevices);

    for (var i = 0; i < this.state.userOp.length; i++) {
      var user = this.state.userOp[i];
      this.selDevices.map(device => {
        this.doLinkPermission(user.value, device.id);
      });
    }

    this.setState({
      submiting: false
    });
  };

  render() {
    let userLines = this.state.users.map(user => {
      return <UserRowWithSelect callback={this.callback} user={user} />;
    });

    let deviceLines = this.state.devices.map(device => {
      return (
        <DeviceRowWithSelect callback={this.callbackDevice} device={device} />
      );
    });

    var userList = this.state.users.map(item => {
      return { value: item.id, label: item.name + " - " + item.email };
    });

    return (
      <div className="animated fadeIn">
        <InputGroup className="col-12">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-user" />
            </InputGroupText>
          </InputGroupAddon>
          <Select
            isMulti
            className="my-col-8"
            value={this.state.userOp}
            options={userList}
            onChange={userOp => {              
              this.setState({ userOp });
            }}
            placeholder="Selecione"
            multi
          />
        </InputGroup>

        <InputGroup className="mb-3 mt-2 col-12">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-directions" />
            </InputGroupText>
          </InputGroupAddon>
          <Input
            name="searchdevice"
            onChange={this.onChangeDevice}
            autoFocus={false}
            type="text"
            placeholder="nome, placa, contato, IMEI"
            autoComplete="search"
            value={this.state.filter}
          />
        </InputGroup>

        <Row className="justify-content-sm-start">
          <Col sm="12" md={{ size: 6, offset: 3 }}>
            <LaddaButton
              className="col-6 btn btn-primary btn-ladda"
              loading={this.state.submiting}
              onClick={e => {
                this.toggleBtn("expLeft");
                this.doSubmit();
              }}
            >
              Confirmar
            </LaddaButton>
          </Col>
        </Row>

        {this.state.showprogress ? (
          <div className="sk-three-bounce">
            <div className="sk-child sk-bounce1" />
            <div className="sk-child sk-bounce2" />
            <div className="sk-child sk-bounce3" />
          </div>
        ) : null}

        {this.state.devices.length != 0 && this.state.showdevices ? (
          <Row style={{ marginTop: "30px" }}>
            <Col>
              <Card>
                <CardBody>
                  <Table noborder hover responsive size="sm">
                    <thead className="thead-light">
                      <tr>
                        <th className="text-center">
                          <i className="icon-directions" />
                        </th>
                        <th />
                        <th>Nome</th>
                        <th>Modelo</th>
                        <th>Fone</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>{deviceLines}</tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        ) : (
          <Row>
            <Col>
              <CardBody />
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPermissions);
