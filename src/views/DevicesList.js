import React, { Component } from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setInvestidor, setEditDevice } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import DeviceRow from "./DeviceRow";
import "spinkit/css/spinkit.css";

class DevicesList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: "",
      show: false,
      devices: []
    };
  }

  searchDevice(filter) {
    this.setState({ show: true });
    axios
      .post(
        "/api/getapicall",
        {
          url: "/devices/search",
          data: "?query=" + filter,
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        this.setState({
          devices: res.data,
          show: false
        });
      });
  }

  componentDidMount() {}

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchDevice(filter);
        } else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = device => {   
    this.props.setEditDevice(device);
    history.push("/app/devices/register");
  };

  render() {
    let deviceLines = this.state.devices.map(device => {
      return <DeviceRow callback={this.callback} device={device} />;
    });

    return (
      <div className="animated fadeIn">
        <InputGroup style={{ width: "50%" }} className="mb-4">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-directions" />
            </InputGroupText>
          </InputGroupAddon>
          <Input
            name="search"
            onChange={this.onChange}
            autoFocus={true}
            type="text"
            placeholder="nome, placa, contato, IMEI"
            autoComplete="search"
          />
        </InputGroup>
        {this.state.show ? (
          <div className="sk-three-bounce">
            <div className="sk-child sk-bounce1" />
            <div className="sk-child sk-bounce2" />
            <div className="sk-child sk-bounce3" />
          </div>
        ) : null}

        {this.state.devices.length != 0 && !this.state.show ? (
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Table noborder hover responsive size="sm">
                    <thead className="thead-light">
                      <tr>
                        <th className="text-center">
                          <i className="icon-directions" />
                        </th>
                        <th>Nome</th>
                        <th>Modelo</th>
                        <th>Placa</th>
                        <th>Fone</th>
                        <th>Operadora</th>
                        <th>Status</th>
                        <th>Ignição</th>
                        <th />
                      </tr>
                    </thead>
                    <tbody>{deviceLines}</tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        ) : (
          <Row>
            
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditDevice
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DevicesList);
