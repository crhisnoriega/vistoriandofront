import { compose, combineReducers, createStore, applyMiddleware } from "redux";

import persistState from "redux-localstorage";

const enhancer = compose(persistState("session"));

// actions.js
export const activateGeod = geod => ({
  type: "ACTIVATE_GEOD",
  geod
});

export const doLogin = session => ({
  type: "DO_LOGIN",
  session
});

export const closeGeod = () => ({
  type: "CLOSE_GEOD"
});

export const setInvestidor2 = session => ({
  type: "SET_INVESTIDOR",
  session
});

export const setProject = session => ({
  type: "SET_PROJECT",
  session
});

export const setToken = session => ({
  type: "SET_TOKEN",
  session
});

export const setWallets = wallets => ({
  type: "SET_WALLETS",
  wallets
});

export const clearSession = session => ({
  type: "CLEAR_SESSION",
  session
});

export const setInvestidor = (investidor, justshow) => ({
  type: "SET_INVESTIDOR",
  investidor,
  justshow
});

export const setEditInvestidor = investidor => ({
  type: "SET_EDIT_INVESTIDOR",
  investidor
});

export const setEditDevice = device => ({
  type: "SET_EDIT_DEVICE",
  device
});

export const setEditUser = user => ({
  type: "SET_EDIT_USER",
  user
});

// reducers.js
export const session = (state = {}, action) => {
  switch (action.type) {
    case "CLEAR_SESSION":
      return {};
    case "SET_WALLETS":
      return { ...state, wallets: action.wallets };
    case "SET_INVESTIDOR":
      return {
        ...state,
        investidor: action.investidor,
        justshow: action.justshow
      };
    case "SET_EDIT_INVESTIDOR":
      return { ...state, edit_investidor: action.investidor };
    case "SET_EDIT_DEVICE":
      return { ...state, deviceEdit: action.device };
    case "SET_EDIT_USER":
      return { ...state, userEdit: action.user };
    case "DO_LOGIN":
      return { ...state, login: action.session };
    case "SET_INVESTIDOR":
      return action.session;
    case "SET_PROJECT":
      return { ...state, project_id: action.session };
    case "SET_TOKEN":
      return { ...state, token: action.session };
    case "ACTIVATE_GEOD":
      return action.geod;
    case "CLOSE_GEOD":
      return {};
    default:
      return state;
  }
};

export const reducer = (state = {}, action) => {
  switch (action.type) {
    case "LOAD_STORED_STATE":
      return action.storedState;
    default:
      return state;
  }
};

export const reducers = combineReducers({
  session,
  reducer
});

const initialState = {};

export const store = createStore(reducers, enhancer);
