import React, { Component } from "react";
import { Bar, Line } from "react-chartjs-2";
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table
} from "reactstrap";
import Widget03 from "../../views/Widgets/Widget03";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import {
  getStyle,
  hexToRgba
} from "@coreui/coreui-pro/dist/js/coreui-utilities";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";

import Websocket from "react-websocket";
import qs from "qs";

import moment from "moment";
import "moment/locale/pt-br";
import ReactTableContainer from "react-table-container";

const brandPrimary = getStyle("--primary");
const brandSuccess = getStyle("--success");
const brandInfo = getStyle("--info");
const brandWarning = getStyle("--warning");
const brandDanger = getStyle("--danger");

// Card Chart 1
const cardChartData1 = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: brandPrimary,
      borderColor: "rgba(255,255,255,.55)",
      data: [65, 59, 84, 84, 51, 55, 40]
    }
  ]
};

const cardChartOpts1 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: "transparent",
          zeroLineColor: "transparent"
        },
        ticks: {
          fontSize: 2,
          fontColor: "transparent"
        }
      }
    ],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData1.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData1.datasets[0].data) + 5
        }
      }
    ]
  },
  elements: {
    line: {
      borderWidth: 1
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4
    }
  }
};

// Card Chart 2
const cardChartData2 = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: brandInfo,
      borderColor: "rgba(255,255,255,.55)",
      data: [1, 18, 9, 17, 34, 22, 11]
    }
  ]
};

const cardChartOpts2 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: "transparent",
          zeroLineColor: "transparent"
        },
        ticks: {
          fontSize: 2,
          fontColor: "transparent"
        }
      }
    ],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData2.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData2.datasets[0].data) + 5
        }
      }
    ]
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4
    }
  }
};

// Card Chart 3
const cardChartData3 = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: "rgba(255,255,255,.2)",
      borderColor: "rgba(255,255,255,.55)",
      data: [78, 81, 80, 45, 34, 12, 40]
    }
  ]
};

const cardChartOpts3 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        display: false
      }
    ],
    yAxes: [
      {
        display: false
      }
    ]
  },
  elements: {
    line: {
      borderWidth: 2
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4
    }
  }
};

// Card Chart 4
const cardChartData4 = {
  labels: [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
    "January",
    "February",
    "March",
    "April"
  ],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: "rgba(255,255,255,.3)",
      borderColor: "transparent",
      data: [78, 81, 80, 45, 34, 12, 40, 75, 34, 89, 32, 68, 54, 72, 18, 98]
    }
  ]
};

const cardChartOpts4 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        display: false,
        barPercentage: 0.6
      }
    ],
    yAxes: [
      {
        display: false
      }
    ]
  }
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 1,
      table: {},
      powerCutEvents: [],
      deviceOnline: [],
      deviceOffline: [],
      showDays: false,
      showHours: false,
      days: 0,
      hours: 0,
      deviceAlreadyOn: []
    };
  }

  componentDidMount() {
    this.setState({ show: true });
    axios
      .post(
        "/api/getapicall",
        {
          url: "/devices/counting",
          data: "",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        this.setState({
          table: res.data
        });
      });

    // setInterval(this.updateEvents, 5000);

    // this.conectWebSocket();
    // this.updateEvents(1);
  }

  conectWebSocket = () => {
    console.log("call");
    axios
      .post(
        "http://35.237.5.236/api/session",
        qs.stringify({
          email: "con4@con4.com.br",
          password: "souancap250669"
        }),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          withCredentials: true
        }
      )
      .then(res => {
        var _this = this;

        var socket = new WebSocket("ws://35.237.5.236/api/socket");
        console.log(socket);
        socket.onopen = function(event) {
          console.log("openWebsocket open");
        };
        socket.onmessage = function(event) {
          var received_msg = event.data;
          var data = JSON.parse(event.data);

          if (data.events != null) {
            data.events.forEach(event => {
              if (
                event.attributes != null &&
                event.attributes.alarm != null &&
                event.attributes.alarm == "powerCut"
              ) {
                console.log(event);
                axios
                  .post(
                    "/api/getapicall",
                    {
                      url: "/positions?id=" + event.positionId,
                      data: "",
                      headers: {
                        "Content-Type": "application/json",
                        Authorization: "Basic " + _this.props.session.token
                      }
                    },
                    {
                      headers: {
                        "Content-Type": "application/json"
                      }
                    }
                  )
                  .then(res => {
                    console.log(res.data);

                    if (res.data.length > 0) {
                      axios
                        .post(
                          "/api/getapicall",
                          {
                            url: "/devices?id=" + res.data[0].deviceId,
                            data: "",
                            headers: {
                              "Content-Type": "application/json",
                              Authorization:
                                "Basic " + _this.props.session.token
                            }
                          },
                          {
                            headers: {
                              "Content-Type": "application/json"
                            }
                          }
                        )
                        .then(res => {
                          console.log(res.data);
                          if (res.data.length > 0) {
                            var device = res.data[0];
                            device.serverTime = event.serverTime;
                            var newlist = [device].concat(
                              _this.state.powerCutEvents
                            );
                            _this.setState({ powerCutEvents: newlist });
                          }
                        });
                    }
                  });
              }
            });
          }
        };

        setInterval(function() {
          console.log(socket.readyState);
        }, 5000);
      })
      .catch(error => {
        console.log(error);
      });
  };

  updateEvents = async (hour, days, type) => {
    console.log("calling");
    console.log("hour: " + hour);
    console.log("days: " + days);

    var query =
      "from=" +
      moment(new Date())
        .subtract(hour, "hour")
        .subtract(days, "day")
        .format() +
      "&to=" +
      moment(new Date()).format() +
      "&type=" +
      type;

    alert(query);

    var res = await axios.post(
      "/api/getapicall",
      {
        url: "/events/period?" + query,
        data: "",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Basic " + this.props.session.token
        }
      },
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    console.log(res.data);

    var _this = this;

    console.log("total retrieve: " + res.data.length);

    var counting = [];

    for (var counter = 0; counter < res.data.length; counter++) {
      var event = res.data[counter];

      if (
        !_this.state.deviceAlreadyOn.includes(event.deviceId) &&
        event.type == "deviceOffline"
      ) {
        _this.state.deviceAlreadyOn.push(event.deviceId);

        if (event.type == "deviceOffline") {
          var res1 = await axios.post(
            "/api/getapicall",
            {
              url: "/devices?id=" + event.deviceId,
              data: "",
              headers: {
                "Content-Type": "application/json",
                Authorization: "Basic " + _this.props.session.token
              }
            },
            {
              headers: {
                "Content-Type": "application/json"
              }
            }
          );

          if (res1.data.length > 0) {
            var device = res1.data[0];

            device.serverTime = event.serverTime;
            device.event = event;

            counting = [device].concat(counting);
          }
        }
      }
    }

    if (type == "deviceOnline") {
      this.setState({ deviceOnline: counting });
    }

    if (type == "deviceOffline") {
      this.setState({ deviceOffline: counting });
    }

    alert("acabou");
  };

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  handleData(data) {
    let result = JSON.parse(data);
    this.setState({ count: this.state.count + result.movement });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected
    });
  }

  getValue = (table, stat) => {
    if (table == null) {
      return "0";
    }

    if (table.offline != null && !stat) {
      return table.offline;
    }

    if (table.online != null && stat) {
      return table.online;
    }
    return "0";
  };

  markAsRead = device => {
    this.setState(prevState => ({
      powerCutEvents: prevState.powerCutEvents.filter(el => el.id != device.id)
    }));
  };

  render() {
    let powerCutEventLines = this.state.powerCutEvents.map(event => {
      return (
        <tr>
          <td className="text-center" />
          <td>
            <div>{event.name}</div>
            <div className="small text-muted">
              <span>powerCut</span>
            </div>
          </td>
          <td className="text-center" />
          <td />
          <td className="text-center">
            {moment(event.serverTime)
              .locale("pt-br")
              .format("LLL")}
          </td>
          <td>
            <Button outline onClick={e => this.markAsRead(event)} color="info">
              Remover
            </Button>
          </td>
        </tr>
      );
    });

    console.log(this.state.deviceOnline.length);

    let deviceOfflineLines = this.state.deviceOffline.map(event => {
      return (
        <tr>
          <td className="text-center" />
          <td>
            <div>{event.name}</div>
            <div className="small text-muted">
              <span>{event.event.type}</span>
            </div>
          </td>
          <td className="text-center" />
          <td />
          <td className="text-center">
            {moment(event.serverTime)
              .locale("pt-br")
              .format("LLL")}
          </td>
          <td>
            <Button outline onClick={e => this.markAsRead(event)} color="info">
              Remover
            </Button>
          </td>
        </tr>
      );
    });

    let deviceOnlineLines = this.state.deviceOnline.map(event => {
      return (
        <tr>
          <td className="text-center" />
          <td>
            <div>{event.name}</div>
            <div className="small text-muted">
              <span>{event.event.type}</span>
            </div>
          </td>
          <td className="text-center" />
          <td />
          <td className="text-center">
            {moment(event.serverTime)
              .locale("pt-br")
              .format("LLL")}
          </td>
          <td>
            <Button outline onClick={e => this.markAsRead(event)} color="info">
              Remover
            </Button>
          </td>
        </tr>
      );
    });

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-warning">
              <CardBody className="pb-0">
                <div className="text-value">
                  Online: {this.getValue(this.state.table.car, true)}
                </div>
                <div className="text-value">
                  Offline: {this.getValue(this.state.table.car, false)}
                </div>

                <div>Carros</div>
                <div
                  className="chart-wrapper mt-3"
                  style={{ height: "50px" }}
                />
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-info">
              <CardBody className="pb-0">
                <div className="text-value">
                  Online: {this.getValue(this.state.table.motorcycle, true)}
                </div>
                <div className="text-value">
                  Offline: {this.getValue(this.state.table.motorcycle, false)}
                </div>

                <div>Motocicletas</div>
                <div
                  className="chart-wrapper mt-3"
                  style={{ height: "50px" }}
                />
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-primary">
              <CardBody className="pb-0">
                <div className="text-value">
                  Online: {this.getValue(this.state.table.van, true)}
                </div>
                <div className="text-value">
                  Offline: {this.getValue(this.state.table.van, false)}
                </div>

                <div>Vans</div>
                <div
                  className="chart-wrapper mt-3"
                  style={{ height: "50px" }}
                />
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-danger">
              <CardBody className="pb-0">
                <div className="text-value">
                  Online: {this.getValue(this.state.table.truck, true)}
                </div>
                <div className="text-value">
                  Offline: {this.getValue(this.state.table.truck, false)}
                </div>

                <div>Caminhões</div>
              </CardBody>
              <div
                className="chart-wrapper mt-3 mx-3"
                style={{ height: "50px" }}
              />
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Dashboard)
);
