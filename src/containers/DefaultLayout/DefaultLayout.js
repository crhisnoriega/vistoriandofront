import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Container } from "reactstrap";

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav
}
from "@coreui/react";
// sidebar nav config
import navigation from "../../_nav";
// routes config
import routes from "../../routes";

import DefaultAside from "./DefaultAside";
import DefaultFooter from "./DefaultFooter";
import DefaultHeader from "./DefaultHeader";

import DefaultHeaderDropdown from "./DefaultHeaderDropdown";

import scriptLoader from "react-async-script-loader";

import { connect } from "react-redux";
import { clearSession } from "../../redux/redux";




class DefaultLayout extends Component {
  render() {
    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader>
              <DefaultHeaderDropdown accnt />
            </AppSidebarHeader>
            <AppSidebarForm />
            <AppSidebarNav navConfig={navigation} {...this.props} />
            <AppSidebarFooter>
              <a className="nav-link" href="/login" onClick={e => {this.props.clearSession();}}>
                <i className="nav-icon fa fa-lock" />
                Sair
              </a>
            </AppSidebarFooter>
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes} />
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                  return route.component ? (
                    <Route
                      key={idx}
                      path={route.path}
                      exact={route.exact}
                      name={route.name}
                      render={props => <route.component {...props} />}
                    />
                  ) : null;
                })}

                <Redirect from="/" to="/app/dashboard" />
              </Switch>
            </Container>
          </main>
          <AppAside fixed hidden>
            <DefaultAside />
          </AppAside>
        </div>
        
      </div>
    );
  }
}



const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  clearSession
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultLayout);
