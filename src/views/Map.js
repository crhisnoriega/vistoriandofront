import React, { Component } from "react";

import { CardBody, Card, Button, Row, Col } from "reactstrap";

import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import Iframe from "react-iframe";

import { connect } from "react-redux";
import { doLogin, setEditUser } from "../redux/redux";

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true
    };
  }

  componentDidMount() {
    alert(JSON.stringify(this.props.session.login.attributes));
    this.setState({ show: true });
  }

  onShow = () => {
    this.setState({ show: true });
  };

  onHide = () => {
    this.setState({ show: false });
  };

  render() {
    return this.props.session.login.attributes.vistoriador ? (
      <Iframe
        className="mymap-size"
        width="100%"
        height="100%"
        url="http://35.237.5.236/mapa/mapa_viewer.html"
        id="myId"
        display="block"
      />
    ) : (
      <Iframe
        className="mymap-size"
        width="100%"
        height="100%"
        url="http://35.237.5.236/mapa/mapa11.html"
        id="myId"
        display="block"
      />
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);
