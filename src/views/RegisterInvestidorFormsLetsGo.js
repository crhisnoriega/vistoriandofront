import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Media,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";
import "./RegisterProjectForms.css";

import axios, { post } from "axios";
import { saveAs } from "file-saver";

import { connect } from "react-redux";
import { doLogin, setInvestidor } from "../redux/redux";
import history from "../history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import logo from "../assets/img/brand/novo_logo_letsgo.png";

const validationSchema = function(values) {
  return Yup.object().shape({
    email: Yup.string()
      .email("Email inválido")
      .required("email de contato"),
    name: Yup.string()
      .min(10, "nome muito curto")
      .required("nome completo requerido"),
    cpf: Yup.string().required("CPF requerido"),

    password: Yup.string().required("senha requerida"),

    rg: Yup.string().required("RG requerido"),

    cep: Yup.string().required("CEP requerido"),

    street: Yup.string().required("rua requerido"),

    number: Yup.string().required("número requerido"),

    accept: Yup.bool()
      .required("* requerido")
      .test("accept", "É necessário aceitar os termos", value => value === true)
  });
};

const validate = getValidationSchema => {
  return values => {
    const validationSchema = getValidationSchema(values);
    try {
      validationSchema.validateSync(values, { abortEarly: false });
      return {};
    } catch (error) {
      return getErrorsFromValidationError(error);
    }
  };
};

const getErrorsFromValidationError = validationError => {
  const FIRST_ERROR = 0;
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR]
    };
  }, {});
};

const initialValues = {
  email: "",
  password: "",
  login: "",
  name: "",
  cpf: "",
  rg: "",
  birth_date: "",
  cep: "",
  street: "",
  number: "",
  city: "",
  state: "",
  imagerg: null,
  login_indicator: null,
  tipoinvest: null,
  project_id: null,
  accept: true,
  indication: null
};

const sleep1 = time => {
  return new Promise(resolve => setTimeout(resolve, time));
};

const onSubmit = (values, { setSubmitting, setErrors }) => {
  values.birth_date = initialValues.birth_date;

  axios
    .post(
      "/api/investidor_register",
      {
        birth_date: values.birth_date,
        email: values.email,
        name: values.name,
        cpf: values.cpf,
        rg: values.rg,
        cep: values.cep,
        number: values.number,
        street: values.street,
        city: values.city,
        state: values.state
      },
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
    .then(res => {
      setSubmitting(false);
    });

  values.email = "";
  values.name = "";
  values.cpf = "";
  values.rg = "";
  values.birth_date = "";
  values.street = "";
  values.number = "";
  values.city = "";
  values.state = "";
  values.accept = false;
  values.cep = "";
};

const fileUpload = file => {
  console.log(file);

  const url = "/api/file-upload";
  const formData = new FormData();
  formData.append("file", file);

  const config = {
    headers: {
      "content-type": "multipart/form-data"
    }
  };
  axios
    .post(url, formData, config)
    .then(function() {
      console.log("SUCCESS!!");
    })
    .catch(function() {
      console.log("FAILURE!!");
    });
};

class RegisterInvestidorForms extends React.Component {
  constructor(props) {
    super(props);

    console.log(this.props.match.params);

    this.touchAll = this.touchAll.bind(this);
    this.state = {
      filename: null,
      imagename: null,
      filevalid: false,
      startDate: moment(),
      indication: null,
      mystreet: "123",
      emystreet: true,
      showdialog: false,
      states: [
        "AC",
        "AL",
        "AM",
        "AP",
        "BA",
        "CE",
        "DF",
        "ES",
        "GO",
        "MA",
        "MG",
        "MS",
        "MT",
        "PA",
        "PB",
        "PE",
        "PI",
        "PR",
        "RJ",
        "RN",
        "RO",
        "RR",
        "RS",
        "SC",
        "SE",
        "SP",
        "TO"
      ],
      indication: {}
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.fileUpload = this.fileUpload.bind(this);
    this.changeRua = this.changeRua.bind(this);
  }

  componentDidMount() {
    axios
      .get(
        "/api/indication/" + this.props.indication,
        {},
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        this.setState({
          indication: res.data
        });
        initialValues.indication = res.data;
      });
  }

  hideDialog() {
    this.setState({ showdialog: false });
  }

  showDialog(isValid) {
    if (isValid) this.setState({ showdialog: true });
  }

  onMyHandlerSubmit(e) {
    this.hideDialog();
  }

  componentWillMount() {}

  handleChange(values) {
    console.log(values);
  }

  handleDate(date) {
    console.log(date);
    initialValues.birth_date = date;
    this.setState({
      startDate: date
    });
  }

  findFirstError(formName, hasError) {
    const form = document.forms[formName];
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus();
        break;
      }
    }
  }

  validateForm(errors) {
    this.findFirstError("simpleForm", fieldName => {
      return Boolean(errors[fieldName]);
    });
  }

  touchAll(setTouched, errors) {
    setTouched({
      name: true,
      cpf: true,
      rg: true,
      birth_date: true,
      cep: true,
      street: true,
      number: true,
      city: true,
      state: true
    });
    this.validateForm(errors);
  }

  onFormSubmit(e) {
    e.preventDefault(); // Stop form submit

    console.log(e);

    this.fileUpload(this.state.file).then(response => {
      console.log(response.data);
    });
  }

  onChange(e) {
    console.log(e.target.files[0]);
    this.setState({ filename: e.target.files[0].name, filevalid: true });
    this.fileUpload(e.target.files[0]).then(response => {
      console.log(response.data);
    });
    initialValues.filename = e.target.files[0];
  }

  onChangeImage(e) {
    console.log(e.target.files[0]);
    this.setState({ imagename: e.target.files[0].name, filevalid: true });
    this.fileUpload(e.target.files[0]).then(response => {
      console.log(response.data);
    });
    initialValues.imagerg = e.target.files[0];
    console.log(initialValues.imagerg);
  }

  handlerCEP(e) {
    alert(this.props.setFieldValue);
    //this.setState({mystreet:"ttee"})
    this.props.setFieldValue("street", "jojojo");
  }

  fileUpload(file) {
    const url = "/api/file-upload";
    const formData = new FormData();
    formData.append("file", file);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    return post(url, formData, config);
  }

  async call(new_cep, values, touched, errors) {
    var res = await axios.get(
      "/api/address/" + new_cep,
      {},
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    );

    var data1 = await res.data;

    console.log(JSON.stringify(data1));
    console.log(data1.logradouro);

    values.street = JSON.parse(data1).logradouro;
    values.state = JSON.parse(data1).uf;
    values.city = JSON.parse(data1).localidade;

    touched.street = true;

    return JSON.parse(data1);
  }

  changeRua(e) {
    this.setState({ mystreet: e.target.value });
  }

  changeEvent(values, touched, errors) {
    console.log(this.state);
    this.setState({ mystreet: "abc" });

    var new_cep = values.cep
      .replace("_", "")
      .replace("_", "")
      .replace("_", "")
      .replace("_", "")
      .replace("_", "")
      .replace("_", "")
      .replace("_", "")
      .replace("_", "")
      .replace("_", "")
      .replace("-", "");

    console.log(new_cep.length);
    console.log(new_cep);

    touched.street = false;
    values.street = "";
    values.state = "";
    values.city = "";
    values.number = "";

    this.call(new_cep, values, touched, errors);
    this.setState({ mystreet: "carregando..." });

    if (new_cep.length == 8) {
      axios
        .get(
          "/api/address/" + new_cep,
          {},
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          var address = JSON.parse(res.data);
          values.street = address.logradouro;
          this.setState({ mystreet: address.logradouro });
          this.setState({ emystreet: false });
        });
    }
  }

  onMySubmit2(values, { setSubmitting, setErrors }) {
    values.birth_date = initialValues.birth_date;

    values.cpf = values.cpf.replace(/\./g, "").replace(/\-/g, "");
    values.cep = values.cep.replace(/\./g, "").replace(/\-/g, "");
    values.rg = values.rg.replace(/\./g, "").replace(/\-/g, "");

    axios
      .post(
        "/api/investidor_register",
        {
          password: values.password,
          birth_date: values.birth_date,
          email: values.email,
          name: values.name,
          cpf: values.cpf,
          rg: values.rg,
          cep: values.cep,
          number: values.number,
          street: values.street,
          city: values.city,
          state: values.state,
          indicate_for: initialValues.indication
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        setSubmitting(false);
        history.push("/ok");
      })
      .catch(error => {
        console.log(error.response);
        history.push("/error");
      });

    values.email = "";
    values.name = "";
    values.cpf = "";
    values.rg = "";
    values.birth_date = "";
    values.street = "";
    values.number = "";
    values.city = "";
    values.state = "";
    values.accept = false;
    values.cep = "";
    values.password = "";
  }

  render() {
    let options = this.state.states.map(ss => {
      return <option value={ss}>{ss}</option>;
    });

    console.log(options);

    return (
      <div className="animated fadeIn">
        <div style={{ height: "20px" }} />
        <Card>
          <CardBody>
            <Row>
              <Col>
                <Media
                  style={{
                    display: "table",
                    margin: "0 auto",
                    width: "120px",
                    height: "95px"
                  }}
                  object
                  src={logo}
                  alt="Generic placeholder image"
                />
                <h2 className="my-title-2">Cadastro de Investidores</h2>
                <div style={{ height: "20px" }} />
              </Col>
            </Row>

            <Formik
              initialValues={initialValues}
              validate={validate(validationSchema)}
              onSubmit={this.onMySubmit2}
              render={({
                values,
                errors,
                touched,
                status,
                dirty,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                handleReset,
                setTouched
              }) => (
                <Row>
                  <Col>
                    <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                      <FormGroup>
                        <Label className="green-label" for="cpf">
                          CPF
                        </Label>
                        <Input
                          style={{ width: "30%" }}
                          type="text"
                          name="cpf"
                          id="cpf"
                          autoComplete="given-cpf"
                          valid={!errors.cpf}
                          invalid={touched.cpf && !!errors.cpf}
                          autoFocus={false}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.cpf}
                          mask="999.999.999-99"
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.cpf}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="name">
                          Nome Completo
                        </Label>
                        <Input
                          style={{ width: "80%" }}
                          type="text"
                          name="name"
                          id="name"
                          autoComplete="given-name"
                          valid={!errors.name}
                          invalid={touched.name && !!errors.name}
                          autoFocus={false}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.name}
                        />
                        <FormFeedback>{errors.name}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="email">
                          Email de contato
                        </Label>
                        <Input
                          style={{ width: "50%" }}
                          type="text"
                          name="email"
                          id="email"
                          autoComplete="given-email"
                          valid={!errors.email}
                          invalid={touched.email && !!errors.email}
                          autoFocus={true}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                        <FormFeedback>{errors.email}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="password">
                          Senha
                        </Label>
                        <Input
                          style={{ width: "50%" }}
                          type="password"
                          name="password"
                          id="password"
                          autoComplete="given-password"
                          valid={!errors.password}
                          invalid={touched.password && !!errors.password}
                          autoFocus={false}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password}
                        />
                        <FormFeedback>{errors.rg}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="rg">
                          RG
                        </Label>
                        <Input
                          style={{ width: "30%" }}
                          type="text"
                          name="rg"
                          id="rg"
                          autoComplete="given-rg"
                          valid={!errors.rg}
                          invalid={touched.rg && !!errors.rg}
                          autoFocus={false}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.rg}
                          mask="99999999999"
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.rg}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="birth_date">
                          Data de Nascimento
                        </Label>
                        <br />

                        <DatePicker
                          style={{ width: "30%" }}
                          selected={this.state.startDate}
                          onChange={this.handleDate.bind(this)}
                          customInput={
                            <Input
                              style={{ width: "80%" }}
                              type="text"
                              name="birth_date"
                              id="birth_date"
                              autoComplete="given-birth_date"
                              valid={true}
                              invalid={false}
                              autoFocus={false}
                              mask="99/99/9999"
                              tag={InputMask}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.birth_date}
                            />
                          }
                          dateFormat="DD/MM/YYYY"
                          locale="pt-BR"
                        />

                        <FormFeedback>{errors.birth_date}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="cep">
                          CEP
                        </Label>
                        <Input
                          style={{ width: "30%" }}
                          type="text"
                          name="cep"
                          id="cep"
                          autoComplete="given-cep"
                          valid={!errors.cep}
                          invalid={touched.cep && !!errors.cep}
                          autoFocus={false}
                          required
                          onChange={e => {
                            handleChange(e);
                          }}
                          onBlur={e => {
                            handleBlur(e);
                            this.changeEvent(values, touched, errors);
                            touched.street = true;
                          }}
                          value={values.cep}
                          mask="99999-999"
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.cep}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="street">
                          Rua
                        </Label>
                        <Input
                          style={{ width: "80%" }}
                          type="text"
                          name="street"
                          id="street"
                          autoComplete="given-street"
                          valid={!errors.street}
                          invalid={touched.street && !!errors.street}
                          autoFocus={false}
                          required
                          onChange={e => {
                            handleChange(e);
                          }}
                          onBlur={e => {
                            handleBlur(e);
                          }}
                          value={values.street}
                        />
                        <FormFeedback>{errors.street}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="number">
                          Numero
                        </Label>
                        <Input
                          style={{ width: "30%" }}
                          type="text"
                          name="number"
                          id="number"
                          autoComplete="given-number"
                          valid={!errors.number}
                          invalid={touched.number && !!errors.number}
                          autoFocus={false}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.number}
                        />
                        <FormFeedback>{errors.number}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="state">
                          Estado
                        </Label>
                        <Input
                          style={{ width: "30%" }}
                          type="select"
                          name="state"
                          id="state"
                          autoComplete="given-state"
                          valid={!errors.state}
                          invalid={touched.state && !!errors.state}
                          autoFocus={false}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.state}
                        >
                          {options}
                        </Input>
                        <FormFeedback>{errors.state}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="city">
                          Cidade
                        </Label>
                        <Input
                          style={{ width: "30%" }}
                          type="text"
                          name="city"
                          id="city"
                          autoComplete="given-city"
                          valid={!errors.city}
                          invalid={touched.city && !!errors.city}
                          autoFocus={false}
                          required
                          onChange={e => {
                            handleChange(e);
                          }}
                          onBlur={handleBlur}
                          value={values.city}
                        />
                        <FormFeedback>{errors.city}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <CustomInput
                          type="checkbox"
                          id="accept"
                          label="Aceito os termos de privacidade"
                          required
                          valid={!errors.accept}
                          invalid={touched.accept && !!errors.accept}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.accept}
                        >
                          <FormFeedback>{errors.accept}</FormFeedback>
                        </CustomInput>
                      </FormGroup>

                      <Row>
                        <FormGroup
                          style={{
                            display: "table",
                            margin: "0 auto",
                            marginLeft: "25%"
                          }}
                        >
                          <Button
                            type="submit"
                            color="success"
                            className="mr-1"
                            disabled={isSubmitting || !isValid}
                            onClick={e => {}}
                          >
                            {isSubmitting ? "Enviando..." : "Confirmar"}
                          </Button>
                          <Button
                            type="reset"
                            color="danger"
                            className="mr-1"
                            onClick={handleReset}
                          >
                            Apagar
                          </Button>
                        </FormGroup>
                      </Row>
                    </Form>
                  </Col>
                </Row>
              )}
            />
          </CardBody>
        </Card>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showdialog}
          toggle={this.toggleInfo}
          className={"modal-success"}
        >
          <ModalHeader toggle={this.toggleInfo}>Cadastro</ModalHeader>
          <ModalBody>Cadastro efetuado com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="success" onClick={e => this.hideDialog()}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

// export default ValidationForms;

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterInvestidorForms);
