import React from "react";
import Loadable from "react-loadable";

function Loading() {
  return <div>Loading...</div>;
}

const Hello = Loadable({
  loader: () => import("./pages/Hello"),
  loading: Loading
});

const Dashboard = Loadable({
  loader: () => import("./views/Dashboard"),
  loading: Loading
});

const Calendar = Loadable({
  loader: () => import("./views/Calendar"),
  loading: Loading
});

const Devices = Loadable({
  loader: () => import("./views/Devices"),
  loading: Loading
});

const DevicesList = Loadable({
  loader: () => import("./views/DevicesList"),
  loading: Loading
});



const Map = Loadable({
  loader: () => import("./views/Map"),
  loading: Loading
});

const UserPermissions = Loadable({
  loader: () => import("./views/UserPermissions"),
  loading: Loading
});

const Reports = Loadable({
  loader: () => import("./views/Reports"),
  loading: Loading
});


const UsersList = Loadable({
  loader: () => import("./views/UsersList"),
  loading: Loading
});

const Users = Loadable({
  loader: () => import("./views/Users"),
  loading: Loading
});


const Surveyors = Loadable({
  loader: () => import("./views/Surveyors"),
  loading: Loading
});

const SurveyorsList = Loadable({
  loader: () => import("./views/SurveyorsList"),
  loading: Loading
});


const Agency = Loadable({
  loader: () => import("./views/Agency"),
  loading: Loading
});

const AgencyList = Loadable({
  loader: () => import("./views/AgencyList"),
  loading: Loading
});


const routes = [
  {
    path: "/app/dashboard",
    name: "Dashboard",
    component: Dashboard,
    exact: true
  },

  {
    path: "/app/calendar",
    name: "Dashboard",
    component: Calendar,
    exact: true
  },
  {
    path: "/app/users/list",
    name: "Usuarios",
    component: UsersList,
    exact: true
  },
  {
    path: "/app/users/register",
    name: "Registrar Usuario",
    component: Users,
    exact: true
  },
  {
    path: "/app/surveyors/list",
    name: "Vistoriadores",
    component: SurveyorsList,
    exact: true
  },
  {
    path: "/app/surveyors/register",
    name: "Registrar Vistoriador",
    component: Surveyors,
    exact: true
  },
  {
    path: "/app/agency/list",
    name: "Imobiliarias",
    component: AgencyList,
    exact: true
  },
  {
    path: "/app/agency/register",
    name: "Registrar Imobiliaria",
    component: Agency,
    exact: true
  }
];

export default routes;
