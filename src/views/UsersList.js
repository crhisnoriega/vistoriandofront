import React, { Component } from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
}
from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditUser } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist//react-bootstrap-table-all.min.css';

import data from './_data';

class UsersList extends Component {
  constructor(props) {
    super(props);

    this.table = data.rows;

    this.state = {
      filter: "",
      show: false,
      users: []
    };
  }

  searchUser(filter) {
    this.setState({ show: true });
    /* axios
       .post(
         "/api/getapicall",
         {
           url: "/users/search?query=" + filter.toLowerCase(),
           data: "",
           headers: {
             "Content-Type": "application/json",
             Authorization: "Basic " + this.props.session.token
           }
         },
         {
           headers: {
             "Content-Type": "application/json"
           }
         }
       )
       .then(res => {
         console.log(res.data);
         
         this.setState({
           users: res.data,
           show: false
         });
       });*/
  }

  componentDidMount() {}

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUser(filter);
        }
        else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.props.setEditUser(user);
    history.push("/app/users/register");
  };

  buttonFormatter = (cell, row) => {
    console.log(cell);
    console.log(row);
    return (<Button className="btn-brand"><i className="fa fa-clipboard"></i></Button>);
  }

  render() {
    let userLines = this.state.users.map(user => {
      return <UserRow callback={this.callback} user={user} />;
    });

    return (
      <div className="animated fadeIn">
      
          <Row>
            <Col>
              <Card>
                <CardHeader>
                   Acessos
                </CardHeader>
                <CardBody>
                
                  
                
                 <BootstrapTable data={this.table} version="4" bordered={ false } striped hover pagination search options={this.options}>
                    <TableHeaderColumn dataField="name" dataSort>Nome</TableHeaderColumn>
                    <TableHeaderColumn isKey dataField="email">CPF</TableHeaderColumn>
                    <TableHeaderColumn dataField="sex" dataSort>Sexo</TableHeaderColumn>
                    <TableHeaderColumn dataField="regDate" dataSort>Data</TableHeaderColumn>
                    
                    <TableHeaderColumn dataField="button" dataFormat={this.buttonFormatter}></TableHeaderColumn>  
                  </BootstrapTable>
          
                  
                  
                  
                  
                </CardBody>
              </Card>
            </Col>
          </Row>
        
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);
